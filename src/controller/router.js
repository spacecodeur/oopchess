import { Router } from 'express';

import makeMoveAction from './Action/Board/makeMoveAction';
import makeMoveValidator from './Validator/makeMoveValidator';
import { newGameAction } from './Action/Board/newGameAction';

const router = Router();

// Board Actions
router.get( '/'         ,                           newGameAction);
router.post('/move'     , ...makeMoveValidator ,    makeMoveAction);