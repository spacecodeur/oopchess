import express from 'express';
import serverConfig from './config/server';
import router from '~/Controller/router';

/**
 * On créé une nouvelle "application" express
 */
const app = express();

/**
 * On dit à Express que l'on souhaite parser le body des requêtes en JSON
 */
app.use(express.json());

app.use(router);

app.listen(serverConfig.API_PORT, () => console.log('Serveur up sur l\'url : http://localhost:' + serverConfig.API_PORT));

// module.exports = app;